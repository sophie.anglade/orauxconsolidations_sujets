# représentation mémoire avec listes et appels de fonctions
l1 = [1,2,[4,5]]
l2 = [6,7,8]
l3 = [l1[2],l2]
#représentation mémoire de l1, l2, l3
print(l1) # 
print(l2) #
print(l3) # [[4,5],[6,7,8]]
l3[1][0] = 10 
l1[2][0] = 12
##représentation mémoire de l1, l2, l3
print(l1) # [1,2,[12,5]]
print(l2) # [10,7,8]
print(l3) # [[12,5],[10,7,8]]

#slicing
print(l3[-1:]) # -> [[10,7,8]]
print(l1[-2 : -1]) #-> [2]
print(l1[2:]) #-> [[12,5]]
print("fin 1")

# appels de fonctions imbriquées
def f1(liste) :
    res = []
    for i in range(len(liste)) :
        if liste[i] %3 == 0 :
            res.append(liste[i] -1)
    return res

def f2(liste) :
    res = []
    for i in range(len(liste)) :
        if liste[i] % 2 == 0 :
            res.append(liste[i] +1)
    return res

def f3(liste) :
    res = 0
    for elem in liste :
        if elem %2 == 0:
            res += elem
        else :
            res -= elem
    return res
l = [1,2,6,4,9,12,11]
#f1(l) -> [5,8,11]
#f2(f1(l)) -> [9]
print(f3(f2(f1(l)))) #-> -9

# (nomMontagne, altitude)
# le nom de la montagne la plus haute
# l'indice de la montagne la plus haute (que changer à la précédente)
# liste des noms de montagnes plus hautes que 2000m
# tri uniquement sur les noms
def plusHaute(liste) :
    if len(liste) == 0 :
        nomM= None
    else:
        altM = liste[0][1]
        nomM = liste[0][0]
        for (nom, alt) in liste[1:] :
            if alt > altM :
                altM = alt
                nomM = nom
    return nomM

def plusHauteIndice(liste) :
    if len(liste) == 0 :
        ind = None
    else:
        ind = 0
        for i in range(1, len(liste)):
            if liste[i][1] > liste[ind][1] :
                ind = i
    return ind

def plusHaute2000(liste) : 
    res = []
    for (nom, alt) in liste :
        if alt > 2000 :
            res.append(nom)
    return res

def supprimePlusHaute(liste, i) :
    res = []
    for j in range(len(liste)) :
        if j != i :
            res.append(liste[j])
    return res

def tri(liste) :
    res = []
    for i in range(len(liste)) :
        ind = plusHauteIndice(liste)
        nom = liste[ind][0]
        liste = supprimePlusHaute(liste,ind)
        res.append(nom)
    return res
#liste avce nom et altitude
def sauver(nomfic, liste) : 
    fic = open(nomfic, 'w')
    for i in range(len(liste)) : 
        ligne = liste[i][0] +','+str(liste[i][1])+'\n'
        fic.write(ligne)
    fic.close()

def restaure(nomfic, liste) : 
    fic = open(nomfic, 'r')
    for ligne in fic :
        l0 = ligne.split(',')
        nom = l0[0]
        alt = int(l0[1])
        liste.append((nom, alt))
    fic.close()

l = [('Mont Fuji', 3776), ('Cervin', 4478), ('Mont Ventoux', 1909),('Everest',8848), ('K2', 8611), ('Annapurna',8091), ('Vignemale', 3298), ('Puy de Sancy', 1886), ('Ballon de Guebwiller', 1424)]
print(plusHaute(l)) #Everest
print(plusHauteIndice(l)) #3
print(plusHaute2000(l)) # ['Mont Fuji', 'Cervin', 'Everest', 'K2', 'Annapurna', 'Vignemale']
print(tri(l)) # ['Everest', 'K2', 'Annapurna', 'Cervin', 'Mont Fuji', 'Vignemale', 'Mont Ventoux', 'Puy de Sancy', 'Ballon de Guebwiller']
sauver('ORAUX1.txt', l)
ll = []
restaure('ORAUX1.txt', ll)
print(ll)