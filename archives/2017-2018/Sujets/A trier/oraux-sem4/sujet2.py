# dictionnaire espèces continent
especes = {}
especes['panda'] = 'Asie'
especes['éléphant'] = 'Asie'
especes['lion'] = 'Afrique'
especes['hippopotame'] = 'Afrique'
especes['écureuil'] = 'Europe'
especes['iguane'] = 'Amérique'
especes['girafe'] = 'Afrique'
print(especes)
# ajouter au dictionnaire le couples ('mésange', 'Europe')
especes['mésange'] = 'Europe'
print(especes)

# Afficher la listes des couples clefs valeurs en utilisant la fonction ad haoc

print(especes.items())

# ecrire la fonction qui permet d'obtenir la liste des couples (clefs, valeur)
def especesContinents(especes) :
    listeEspCont = []
    for (esp, cont) in especes.items() :
        listeEspCont.append((esp, cont))
    return listeEspCont

print(especesContinents(especes))

#définir un dictionnaire avec le nombre d'espèces
# par continent 

def continentNbreEspeces(especes) :
    nbEspeces = {}
    for (esp, cont) in especes.items() :
        if cont in nbEspeces :
            nbEspeces[cont] += 1
        else : 
            nbEspeces[cont] = 1
    return nbEspeces

print(continentNbreEspeces(especes))

# à l'aide de la fonction précédente déterminer le
# continent ayant le plus grand nombre d'espèces.

def continentMaxEspeces(especes) :
    nbEspeces = continentNbreEspeces(especes)
    contMax = None
    nbMax = 0
    for(cont, nb) in nbEspeces.items() :
        if nb > nbMax :
            nbMax = nb
            contMax = cont
    return contMax

print(continentMaxEspeces(especes))

#on possède un dictionnaire ayant pour chaque
# espèce le nombre de représentants

nombreRepresentants = {}
nombreRepresentants['panda'] = 500
nombreRepresentants['éléphant'] = 250
nombreRepresentants['lion'] = 1000
nombreRepresentants['hippopotame'] = 890
nombreRepresentants['écureuil'] = 2000000
nombreRepresentants['iguane'] = 1450
nombreRepresentants['girafe'] = 2580
nombreRepresentants['mésange'] = 2580
print(nombreRepresentants)
# dictionnaire pour chaque continent la liste
# especes avec leur nombre
# {continent : [(espece, nb),(espece,nb)]}

def continentEspecesNb(especes, nombreRepresentant) :
    contEspNb = {}
    for (esp, cont) in especes.items() :
        if cont not in contEspNb :
            contEspNb[cont] = [(esp, nombreRepresentants[esp])]
        else :
            contEspNb[cont].append((esp,nombreRepresentants[esp]))
    return contEspNb

print(continentEspecesNb(especes, nombreRepresentants))

#quel est le continent qui a le plus grand nombre
# de représentants.
def max(dico) :
    max = 0
    ContMax = None
    for (cont, nombre) in dico.items() : 
        if nombre > max :
            max = nombre
            contMax = cont
    return contMax

def continentPlusGrandNbEspeces(especes, nombreRepresentants) :
    dico = continentEspecesNb(especes, nombreRepresentants)
    # transforme dico en un dico (cont, nbretotal)
    dicoMax = {}
    for (cont, liste) in dico.items() :
        n = 0 
        for (esp, nb) in liste :
            n += nb
        dicoMax[cont] = n
    print(dicoMax)
    return max(dicoMax)

print(continentPlusGrandNbEspeces(especes, nombreRepresentants))
        
