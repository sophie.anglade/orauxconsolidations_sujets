#dictionnaire champignon veneneux (True) ou pas (False) et région de présence

champignons = {}
champignons['amanite phalloïde'] = (True, 'pyrénées orientales')
champignons['cèpe'] = (False, 'le midi')
champignons['cortinaire'] = (True, 'région parisienne')
champignons['gyromitre'] = (True, 'massif central')
champignons['lactaire délicieux'] = (False, 'pyrénées orientales')
champignons['girolle'] = (False, 'Seine maritime')
champignons['trompette des morts'] = (False, "côte d'or")

print(champignons)

# ajouter le champignon non vénéneux morille que l'on trouve en farnche Comté
champignons['morille'] = (False, 'Seine maritime')

# donner les listes des valeurs du dictionnaire champignons en utilisant une méthodes ad hoc

print(champignons.values())

# idem avec une fonction 
def valeursChampignons(champignons) :
    valChampignons = []
    for(_, tuple) in champignons.items() :
        valChampignons.append(tuple)
    return valChampignons

print(valeursChampignons(champignons))

# dico des vénéneux avec leur habitat et des non avec habitats
def lesVeneneux(champignons) : 
    veneneux = {}
    nonVeneneux = {}
    for (nom, (ven, hab)) in champignons.items() :
        if ven :
            veneneux[nom] = hab
        else :
            nonVeneneux[nom] = hab
    return (veneneux,nonVeneneux)

(veneneux, nonVeneneux) = lesVeneneux(champignons)
print(" les veneneux ")
print(veneneux)
print(" les non veneneux ")
print(nonVeneneux)

#dico des champignons d'une région
# quelle représentation ('region', [noms champignons]) 
def champignonsRegion(champignons) :
    champReg = {}
    for (nom, (_, hab)) in champignons.items() :
        if hab not in champReg :
            champReg[hab] = [nom]
        else :
            champReg[hab].append(nom)
    return champReg

print(champignonsRegion(champignons))

# champignons et types ('à lames', 'à plis', 'à tubes', 'à formes non classiques')

typesChampignons = {}
typesChampignons['amanite phalloïde'] = 'à lames'
typesChampignons['cèpe'] = 'à tubes'
typesChampignons['cortinaire'] = 'à lames'
typesChampignons['gyromitre'] = 'à formes non classiques'
typesChampignons['lactaire délicieux'] = 'à lames'
typesChampignons['girolle'] = 'à plis'
typesChampignons['trompette des morts'] = 'à plis'
typesChampignons['morille'] = 'à formes non classiques'
print(typesChampignons)

# dictionnaires donnant par types la liste des champignons veneneux ou pas
def typesChampignonsNoms(champignons, typesChampignons) :
    dico = {}
    for (nom, type) in typesChampignons.items() :
        if type not in dico :
            dico[type] = [(nom, champignons[nom][0])]
        else :
            dico[type].append((nom, champignons[nom][0]))
    return dico

print(typesChampignonsNoms(champignons, typesChampignons))

# le type ayant le maximum de champignons en utilisant la fonction précédente
def maxChampignonsParType(champignons, typesChampignons) :
    dico = typesChampignonsNoms(champignons, typesChampignons)
    type = None
    max = 0
    for (typ, liste) in dico.items() :
        if len(liste) > max :
            max = len(liste)
            type = typ
    return type

print(maxChampignonsParType(champignons, typesChampignons))
