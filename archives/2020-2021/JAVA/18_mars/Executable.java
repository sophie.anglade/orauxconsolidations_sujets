public class Executable{

    public static void main(String [] args){
    
        Aquarium  aqua = new Aquarium();
        aqua.ajouter(new Poisson(2, 4));
        aqua.ajouter(new Poisson(5, 9));
        aqua.ajouter(new Bulle(5, 6));
        aqua.animer();
    }
}
