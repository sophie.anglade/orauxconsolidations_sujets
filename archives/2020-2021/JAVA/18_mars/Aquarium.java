import java.util.List;
import java.util.ArrayList;


public class Aquarium{
    // private List<Poisson> lesPoissons;
    // private List<Bulle> lesBulles;
    // private List<Algue> lesAlgues;
    private List<Aquariumable> leContenu;
    
    public Aquarium(){
        // this.lesPoissons = new ArrayList<>();
        // this.lesBulles = new ArrayList<>();
        this.leContenu = new ArrayList<>();
    }
    
    // public void ajouter(Poisson poisson){
        // this.lesPoissons.add(poisson);
    // }

    // public void ajouter(Bulle bulle){
        // this.lesBulles.add(bulle);
    // }
    
    public void ajouter(Aquariumable truc){
        this.leContenu.add(truc);
    }
    
    public void animer(){
        // for(Poisson poiss : this.lesPoissons){
            // poiss.avancer();
        // }
        // for(Bulle b : this.lesBulles){
            // b.avancer();
        // }   
        for(Aquariumable truc : this.leContenu){
            truc.avancer();
        } 
    }
}
