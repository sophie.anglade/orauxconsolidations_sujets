public class Bulle implements Aquariumable{
    private int posX, posY;
    
    public Bulle(int x, int y){
        this.posX = x;
        this.posY = y;
    }
    
    public void avancer(){
        this.posY = this.posY+1;
    }

}
